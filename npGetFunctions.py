from getFunctions import *

### NARODNO POZORISTE
def npGetAuthor(e):
    if e is not None:
        for k in e.itertext():
            m = re.match(r"(\)\s*,\s*)(.*)", k)
            if m is not None:
                return m.group(2)

def npGetDate(e):
    if e is not None:
        datum = dateutil.parser.parse(e.get('datetime'))
        return datum
    else:
        return ""

# mozda treba da promeni ime u npShowAll
def npGetInfo(e):
    nameE = e.find(".//span[@itemprop='name']")
    genreE = e.find(".//span[@itemprop='eventType']")
    dateE = e.find(".//time")
    authorE = e.find(".//small[@class='small2']")

    name = checkNone(getElementText(nameE))
    genre = checkNone(getElementText(genreE))
    date = checkNone(getAttributeValue(dateE, 'datetime'))
    author = checkNone(npGetAuthor(authorE))


    return [name, genre, author, date]