import dateutil.parser

def npPrintDate(e, prefix):
    if e is not None:
        datum = dateutil.parser.parse(e.get('datetime'))
        print prefix + str(datum.strftime("%d.%m.%Y. u %H:%M:%S"))
    else:
        print prefix + "/"