from lxml import html
from printFunctions import *
from filterFunctions import *

from npGetFunctions import *
from psGetFunctioins import *


printInstitution("Narodno Pozoriste")

#link za krolovanje
url = "http://www.narodnopozoriste.rs/en/repertoire"
tree = html.parse(url)
root = tree.getroot()


for i in root.findall(".//div[@class='panel-heading']"):
    #name, genre, author, date
    info = npGetInfo(i)

    #printSingleEvent(info[0],info[1], info[2], info[3])


    Find(info[0],info[1], info[2], info[3], 'carmen')
    #FindByName(info[0],info[1], info[2], info[3], 'carmen')
    #FindByAuthor(info[0],info[1], info[2], info[3],'Maxim')
    #FindByGenre(info[0],info[1], info[2], info[3], 'Ballet')


printInstitution("Pozoriste Slavija")

url = "http://www.pozoriste-slavija.co.rs/"
tree = html.parse(url)
root = tree.getroot()

for i in root.findall(".//tr[@class='style59']"):
    # name, genre, author, date
    info = psGetInfo(i)

    #printSingleEvent(info[0], info[1], info[2], info[3])

    Find(info[0], info[1], info[2], info[3], 'carmen')
    #FindByName(info[0], info[1], info[2], info[3], 'karamazovi')
    #FindByAuthor(info[0],info[1], info[2], info[3],'dostojevski')
