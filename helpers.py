import re
import datetime

def getMonthNumber(m):
    return ["", "JAN", "FEB", "MAR", "APR", "MAJ", "JUN", "JUL", "AVG", "SEP", "OKT", "NOV", "DEC"].index(m)

### Pozoriste Slavija

#create date string from website data
def psCreateDate(e):
    for k in  e.itertext():
        m = re.match(r"([0-9]*)[.](.*)", k)
        if m is not None:
            day = m.group(1)
            month = m.group(2).strip()[:3]
            year = datetime.datetime.now().year
            date = datetime.datetime(year, getMonthNumber(month), int(day))
            return date

def psUpdateDate(e, date):
    for b in e.itertext():
        m = re.match(r"([0-9]+)[.]([0-9]+)", b)

        if m is not None:
            date = date.replace(hour=int(m.group(1)), minute=int(m.group(2)))
    return date

def checkNone(e):
    if e is None:
        return "/"
    else:
        return e