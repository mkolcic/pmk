import dateutil.parser
from printFunctions import *
from helpers import *
import re

# Prints text from some html element
def getElementText(e):
    if e is not None and e.text is not None:
        return e.text
    else:
        return "/"

def getAttributeValue(e, attr):
    if e is not None:
        return e.get(attr)
    else:
        return "/"



