from getFunctions import *

# TODO @prefect refaktorisi
def psGetInfo(e):
    for i, k in enumerate(e.findall(".//td")):
        # date td
        if i == 0:
            date = psGetDate(k)
        # name td
        elif i == 1:
            name = psGetName(k)
        #third td
        elif i == 2:
            # Tekst rezija
            author = psGetAuthor(k)
        # time td
        elif i == 3:
            date = psUpdateDate(e, date)

    if date == "/":
        # TODO @prefect ili je html los ili je lose krolovano
        return [checkNone(name), "/", checkNone(author), "/"]
    else:
        return [checkNone(name), "/", checkNone(author), checkNone(date.isoformat())]


def psGetName(e):
    for k in e.iter('span'):
        return checkNone(k.text)

def psGetDate(e):
    return checkNone(psCreateDate(e))

def psGetAuthor(e):
    return checkNone(getElementText(e))
