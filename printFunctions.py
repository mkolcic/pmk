import dateutil.parser
import re

# Prints text from some html element
def printElementText(e, prefix):
    if e is not None and e.text is not None:
        print prefix + e.text
    else:
        print prefix + "/"

# Prints value of elements attr
def printAttributeValue(e, attr):
    if e is not None:
        print e.get(attr)
    else:
        print "/"

def printInstitution(name):
    print "==========================="
    print name.upper()
    print "==========================="

def printSingleEvent(name, genre, author, date):

    if name == "/" or date == "/":
        print ""
    else:
        datum = dateutil.parser.parse(date)

        print "---------------------------"
        print "Ime: " + name
        print "Zanr: " + genre
        print "Datum: " + str(datum.strftime("%d.%m.%Y. u %H:%M:%S\n")),
        print "Autor: " + author
        print "---------------------------"

# Prints some string with some prefix
def printStringPrefix(string, prefix):
    if string is not None:
        print prefix + string
    else:
        print prefix + "/"

