from printFunctions import *
from helpers import *


def Find(name, genre, author, date, criterion):
    tmpLower = map(lambda x: x.lower(), [name, genre, author, criterion])
    if any(s.find(tmpLower[3]) != -1 for s in tmpLower[:3]):
        printSingleEvent(name, genre, author, date)

def FindByName(name, genre, author, date, criterion):
    nameLower = name.lower()
    if nameLower.find(criterion.lower()) != -1:
        printSingleEvent(name, genre, author, date)

def FindByAuthor(name, genre, author, date, criterion):
    autorLower = author.lower()
    if autorLower.find(criterion.lower()) != -1:
        printSingleEvent(name, genre, author, date)

def FindByGenre(name, genre, author, date, criterion):
    genreLower = genre.lower()
    if genreLower.find(criterion.lower()) != -1:
        printSingleEvent(name, genre, author, date)

